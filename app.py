from flask import Flask, g
from flask.ext.login import LoginManager

import models

DEBUG = True
PORT = 8000
HOST = '0.0.0.0'

app = Flask(__name__)
app.secret_key = 'akeibkaieuba39a,alsg9a9aa.sg9eahx.gjkawofgkla!'

login_manager = LoginManager() # a utility for handling user authentication
login_manager.init_app(app)
login_manager.login_view = 'login'

@login_manager.user_loader
def load_user(userid): # login manager will use this to look up a user
    try:
        return models.User.get(models.User.id == userid)
    except models.DoesNotExist: # from peewee - if no record is returned
        return None

@app.before_request # run this function before the request hits the view
def before_request():
    """Connect to the database before each request."""
    # g is a flask global object that retains the database and
    # connection status as well as information about our app
    g.db = models.database
    g.db.connect()


@app.after_request # run this function before the response is returned
def after_request(response):
    """Close the database connection after each request."""
    g.db.close()
    return response





# make sure this only runs as a standalone script &
# setup our app run values as variables so we can change them easily
# up ^^^ There
if __name__ == '__main__':
    models.initialize()
    models.User.create_user(
        name='Alex Rodriguez',
        email='alexr@rodspace.com',
        password=kitfox1701,
        admin=True
    )
    app.run(debug=DEBUG, host=HOST, port=PORT)

