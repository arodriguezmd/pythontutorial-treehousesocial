import datetime

from flask.ext.login import UserMixin
from flask.ext.bcrypt import generate_password_hash, check_password_hash
from peewee import *  # The convention is to import all of peewee

DATABASE = SqliteDatabase('social.db')

class User(UserMixin, Model):

    # NB: UserMixin is a class that provides some additonal functionality
    # and needs to be part of our inheritance chain

    username = CharField(unique=True)
    email = CharField(unique=True)
    # Hash library will use about 60 characters for password field but might change
    # this library in the future so let's allow for growth
    password = CharField(max_length=100)
    joined_at = DateTimeField(default=datetime.datetime.now) # import datetime library
    is_admin = BooleanField(default=False)

    email = CharField(unique=True)
    password = CharField(max_length=100)
    join_date = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = DATABASE
        # need the comma because this is a tuple, the '-' is to sort in descending order
        order_by = ('-joined_at',)

    # This will create the model instance and then use it where the cls
    @classmethod
    def create_user(cls, username, email, password, admin=False):
        try:
            cls.create(
                username=username,
                email=email,
                password=generate_password_hash(password),
                is_admin=admin)
        except IntegrityError:
            # this error will occur when username is not unique
            raise ValueError("User already exists")

    def initialize():
        DATABASE.connect()
        DATABASE.create_tables([User], safe=True)
        DATABASE.close()



